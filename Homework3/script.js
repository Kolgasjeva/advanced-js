/*
Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна.
Деструктуризація в JavaScript — це зручний спосіб отримання окремих елементів або властивостей з складних структур даних, таких як об'єкти або масиви, шляхом розбиття їх на окремі змінні.
У багатьох випадках, коли ми працюємо з об'єктами або масивами, нам потрібно отримати доступ до окремих елементів цих структур. 
Зазвичай це робиться шляхом використання  для об'єктів - object.property, у для масивів  - array[index]. 
Однак, коли структура складається з багатьох елементів, цей підхід може стати незручним і призводити до багато повторюваного коду.
Деструктуризація дозволяє нам одночасно оголосити і присвоїти значення кільком змінним.
*/

// Завдання 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let unitedArray = [...clients1, ...clients2]

function filterUniqueValues(array) {
    const mergedArray = array.filter((item, index, array) => {
        return array.indexOf(item) === index;
    })
    return mergedArray
}

const filtredArray = filterUniqueValues(unitedArray);
console.log(filtredArray);

// Завдання 2

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

  const charactersShortInfo = [];

  characters.forEach(character => {
    const {name, lastName, age} = character;
    charactersShortInfo.push({name, lastName, age});
  })

  console.log(charactersShortInfo);

// Завдання 3

const user1 = {
    name: "John",
    years: 30
  };

  ({name, years, isAdmin = false} = user1 );

  console.log(name, years, isAdmin);

  // Завдання 4

  const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

  function mergeObject(...rest) {
    let mergedObject = {};
    rest.forEach(obj => {
        mergedObject = {...mergedObject, ...obj}
    })
    return mergedObject;
  }

  const mergedObject = mergeObject(satoshi2018, satoshi2019, satoshi2020);
  console.log(mergedObject);

  // Завдання 5

  const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  const mergedBooks = [...books, bookToAdd];

  console.log(mergedBooks);

// Завдання 6

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const newEmployee = {...employee, age: '', salary: ''}

console.log(newEmployee);

// Завдання 7

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value);
alert(showValue());  

 
