let usersArray = '';
let postsArray = '';
const wrapperPost = document.createElement('div');
wrapperPost.classList.add('posts-wrapper')
document.body.append(wrapperPost);

function getUsersPost() {
    const usersPromise = fetch('https://ajax.test-danit.com/api/json/users')
        .then(response => response.json())
        .then(users => {
            return usersArray = users;
        })

    const postsPromise = fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => response.json())
        .then(posts => {
            return postsArray = posts;
        })

    return Promise.all([usersPromise, postsPromise])
}


class Card {
    constructor (user, post) {
        this.user = user;
        this.post = post;
        this.card = this.createPosts();
    }
    
    createPosts () {
        const card = document.createElement('div');
        card.classList.add('container-post')
        card.innerHTML = `<p class="containet-post__user-name">${this.user.name}</p>
       <p class="containet-post__user-email">Email: ${this.user.email}</p>
       <h2 class="containet-post__title">${this.post.title}</h2>
       <p class="containet-post__text">${this.post.body}</p>`

        const deleteButton = document.createElement('button')
        deleteButton.classList.add('containet-post__delete-button');
        deleteButton.innerText = 'Delete';
        card.append(deleteButton);
        wrapperPost.append(card);

        deleteButton.addEventListener('click',() => this.deletePosts())

        return card;
    }

    deletePosts() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE',
        })
            .then(response =>{
                if(response.ok) {
                    this.card.remove()
                }
            })
            .catch (err => 
                console.log(err)
            )
    }
}

function createCard(usersArray, postsArray) {
    postsArray.forEach(post => {
        const user = usersArray.find(user => user.id === post.userId);
        const card = new Card(user, post);
    });
}

getUsersPost()
    .then(post =>  createCard(usersArray, postsArray));




