/*
1) Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
Під час отримання даних від користувача можуть виникати помилки, через введення некоректних або неповних даних. 
Під час маніпуляцій з елементами DOM, такими як отримання значення поля вводу або доступ до неіснуючого елемента.
Робота зі сторонніми бібліотеками: Під час використання сторонніх бібліотек, можуть виникати непередбачувані помилки, які можуть зупинити виконання коду. 
Робота з парсерами JSON: Під час парсингу рядка JSON можуть виникати синтаксичні помилки, які важко передбачити наперед. 
Використання try...catch дозволяє відловити такі помилки і обробити їх без зупинки виконання програми.
*/



const block = document.createElement('div');
document.body.prepend(block);
block.id = 'root';


function renderBooks(array) {
  const listElement = document.createElement('ul');

  array.forEach((obj, indexObject) => {
    try {
      const requiredProperties = ['author', 'name', 'price'];
      for (let i = 0; i < requiredProperties.length; i++) {
        const property = requiredProperties[i];
        if (!obj.hasOwnProperty(property)) {
          throw new SyntaxError(`Властивість ${property} у елементі масиву з індексом ${indexObject} відсутня`);
        }
      }
      const content = `<li>Author: ${obj.author}<br> Name: ${obj.name}<br>Price: ${obj.price}</li>`
      listElement.insertAdjacentHTML('beforeend', content);
      block.append(listElement);
    }
    catch (e) {
      console.log('Помилка' + ": " + e.message)
    }
  })
}

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

renderBooks(books)

