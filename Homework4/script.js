/*
1) Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
AJAX (Asynchronous JavaScript and XML) - це технологія, що дозволяє взаємодіяти з сервером без перезавантаження сторінки. 
За допомогою AJAX, веб-сторінка може асинхронно обмінюватися даними з сервером, отримувати нову інформацію та оновлювати вміст сторінки без необхідності повного перезавантаження.

Основна перевага використання AJAX при розробці JavaScript полягає:
1) Асинхронність: AJAX дозволяє виконувати запити до сервера асинхронно, тобто без блокування виконання інших операцій. 
Це означає, що користувач може продовжувати взаємодіяти зі сторінкою, поки відбувається обмін даними з сервером.
2) Оновлення частини сторінки: Завдяки AJAX, ви можете оновлювати лише потрібну частину сторінки, не перезавантажуючи всю сторінку. 
3)Розширені можливості валідації та валідність даних: AJAX дозволяє валідувати дані на боці клієнта перед їх відправкою на сервер. 
Це дозволяє зменшити кількість неправильно введених або некоректних даних, що надходять на сервер.
*/

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(list => {
        const listFilms = document.createElement('div');
        document.body.append(listFilms);

        list.forEach(element => {
            const itemFilm = document.createElement('div');
            itemFilm.innerHTML = `<h2 data-name='${element.name}'>Назва фільму:${element.name}</h2>
        <p>Номер епізоду: ${element.episodeId}</p>
        <p>Короткий зміст: ${element.openingCrawl}</p>`
            listFilms.append(itemFilm);

            const characters = element.characters;

            characters.forEach(character => {
                fetch(character)
                    .then(response => response.json())
                    .then(actor => {
                        const filmName = document.querySelector(`[data-name ='${element.name}']`);
                        const personDescriotion = document.createElement('p');
                        for (key in actor) {
                            personDescriotion.innerText += `${key}: ${actor[key]}; `;
                        }
                        filmName.insertAdjacentElement('afterend', personDescriotion);
                    })
            })
        })
    });