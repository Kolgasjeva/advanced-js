
/*
Асинхронність у JavaScript означає, що певні операції або функції можуть виконуватися паралельно або у фоновому режимі, без блокування виконання коду. 
Зазвичай, коли ми виконуємо синхронний код, кожна операція виконується послідовно, чекаючи на закінчення попередньої операції, перш ніж перейти до наступної.
Проте, у JavaScript ми також маємо можливість виконувати асинхронний код. 
Наприклад, коли ми взаємодіємо з сервером і чекаємо на відповідь, ми не хочемо блокувати виконання іншого коду, тому ми використовуємо асинхронні операції.
Основний механізм асинхронності в JavaScript - це використання функцій зворотного виклику (callbacks), промісів (promises) або async/await. 
Замість того, щоб чекати на результат операції, JavaScript продовжує виконувати наступний код. 
Коли операція завершується, виконується функція зворотного виклику або виконується обробка промісу.
Наприклад, якщо ми виконуємо запит до сервера для отримання даних, замість блокування виконання коду і чекання на результат, ми можемо використовувати асинхронні операції, такі як функції зворотного виклику або проміси, щоб вказати, що коли дані будуть готові, вони повинні бути оброблені.
*/
const btn = document.createElement('btn');
btn.innerText = 'Знайти по IP';
btn.classList.add('btn')
document.body.append(btn);

btn.addEventListener('click', async () => {
    try {
        const IPAddress = await (await fetch('https://api.ipify.org/?format=json')).json();
        const userLocation = await (await fetch(`http://ip-api.com/json/${IPAddress.ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`)).json();
        showLocation(userLocation);
    } catch (err) {
        console.log(err)
    }
})

function showLocation(location) {
    const wrapperLocation = document.createElement('div');
    wrapperLocation.innerHTML = `<ul>
    <li>Континент: ${location.continent}</li>
    <li>Країна: ${location.country}</li>
    <li>Регіон: ${location.regionName}, ${location.region}</li>
    <li>Місто: ${location.city}</li>
    <li>Район: ${location.district ? location.district : '-'}</li>
    </ul>`
    document.body.append(wrapperLocation)
}
