/*Теоретичне питання 
1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
В JavaScript кожен об'єкт має посилання на свій "прототип" - це об'єкт, з якого він успадковує властивості та методи. 
Коли ми намагаємося отримати доступ до властивості або методу об'єкта, JavaScript перевіряє, чи є ця властивість у самому об'єкті. 
Якщо вона не знайдена, JavaScript переглядає прототип об'єкта, і так далі, поки не знайде відповідну властивість або метод або не дійде до кінця ланцюжка прототипів.
Механізм прототипного наслідування в JavaScript заснований на двох властивостях об'єктів: __proto__ і prototype:
1)__proto__ - це властивість, яка зберігає посилання на прототип об'єкта. 
Коли ми спробуємо отримати доступ до властивості або методу об'єкта, JavaScript шукатиме його у самому об'єкті, а якщо не знайде, перейде до його прототипу.
2)Крім того, кожна функція в JavaScript має властивість prototype.
Властивість prototype містить об'єкт, і всі об'єкти, створені з цієї функції-конструктора, будуть мати __proto__, який посилається на цей об'єкт prototype. 
Таким чином, об'єкти, створені з однієї функції-конструктора, будуть спадковувати властивості та методи з цього prototype.

Загалом, прототипне наслідування в JavaScript дозволяє об'єктам спадковувати властивості та методи від їх прототипів. 

2) Для чого потрібно викликати super() у конструкторі класу-нащадка?
Виклик super() у конструкторі класу-нащадка виконує конструктор батьківського класу. 
Це робиться для того, щоб передати вхідні аргументи від класу-нащадка до конструктора батьківського класу і забезпечити правильну ініціалізацію спільних властивостей.

Коли клас успадковується від іншого класу, він може мати свої додаткові властивості та методи, але також може спадкувати властивості та методи від батьківського класу. 
Виклик super() у конструкторі дозволяє налаштувати спільні властивості, які є частиною батьківського класу, та передати необхідні значення для ініціалізації цих властивостей.

*/

class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name (value) {
        this._name = value;
    }
    get name () {
        return this._name;
    }
    set age (value) {
        this._age = value;
    }
    get age () {
        return this._age;
    }
    set salary (value) {
        this._salary = value;
    }
    get salary () {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary () {
        return this._salary*3;
    }
}

const programmer1 = new Programmer("Alice", 30, 50000, ["Python","C++"]);
const programmer2 = new Programmer("Helen", 24, 60000, ["Java", "JavaScript"]);
const programmer3 = new Programmer("Jack", 35, 65000, ["PHP", "JavaScript"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

//const Alyona = new Employee ('Alyona', 24, 40000)
//console.log(Alyona);
//Alyona.name = "Лелик";
//console.log(Alyona._name)
//const progAlyona = new Programmer ('Alyona', 24, 40000,'ru,ua')
//console.log(progAlyona);
//progAlyona._salary = 45000;
//const salary = progAlyona.salary;
//progAlyona._salary = 45000;
//console.log(salary);